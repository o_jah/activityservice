package com.sysomos.activity.service;

import com.sysomos.activity.service.GetUserActivity.ResultContext;
import com.sysomos.activity.service.utils.ListUtils;

/**
 * @author adia
 */
public class ActivityService {

	/**
	 * Type of engagement between twitter users
	 */
	public static enum ActivityType {

		RETWEET(0), REPLY(1), MENTION(2);

		int descriptor;

		private ActivityType(int descriptor) {
			this.descriptor = descriptor;
		}

		public static ActivityType get(int descriptor) {
			for (ActivityType type : values()) {
				if (type.descriptor == descriptor)
					return type;
			}
			return null;
		}
	}

	private static final int BATCH_SIZE = 300;

	/**
	 * This method returns an instance of {@link GetUserActivity.Request}. To
	 * get the activity of the user given in input, use this instance to call
	 * {@link com.sysomos.activity.service.GetUserActivity.Request#call()}. The
	 * activity is wrapped into a {@link ResultContext}. Below is an example of
	 * how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code Pair<Long, Integer>} pair = resultContext.get(0);
	 * int activity = pair.getB();
	 * </code>
	 * </pre>
	 * 
	 * @param userId
	 *            User that is engaging with another user post
	 * @return GetUserActivity.Request
	 */
	public static GetUserActivity.Request newGetUserActivityRequest(long userId) {
		return new GetUserActivity.Request(userId);
	}

	/**
	 * This method returns an instance of {@link GetUserActivity.Request}. To
	 * get the activity of the user given in input, use this instance to call
	 * {@link com.sysomos.activity.service.GetUserActivity.Request#call()}. The
	 * activity is wrapped into a {@link ResultContext}. Below is an example of
	 * how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code Pair<Long, Integer>} pair = resultContext.get(0);
	 * int activity = pair.getB();
	 * </code>
	 * </pre>
	 * 
	 * @param userId
	 *            User that is engaging with another user post
	 * @param type
	 *            Activity type (@see {@link ActivityService.ActivityType})
	 * @return GetUserActivity.Request
	 */
	public static GetUserActivity.Request newGetUserActivityRequest(
			long userId, final ActivityType type) {
		return new GetUserActivity.Request(userId, type.descriptor);
	}

	/**
	 * This method returns an instance of {@link GetUserActivity.Request}. To
	 * get the activity of the user given in input, use this instance to call
	 * {@link com.sysomos.activity.service.GetUserActivity.Request#call()}. The
	 * activity is wrapped into a {@link ResultContext}. Below is an example of
	 * how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code Pair<Long, Integer>} pair = resultContext.get(0);
	 * int activity = pair.getB();
	 * </code>
	 * </pre>
	 * 
	 * @param userId
	 *            User that is engaging with other users posts
	 * @param startDateMillis
	 *            The start date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @param endDateMillis
	 *            The end date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @param type
	 *            Activity type (@see {@link ActivityService.ActivityType})
	 * @return GetUserActivity.Request
	 */
	public static GetUserActivity.Request newGetUserActivityRequest(
			long userId, long startDateMillis, long endDateMillis,
			final ActivityType type) {
		return new GetUserActivity.Request(userId, type.descriptor,
				startDateMillis, endDateMillis);
	}

	/**
	 * This method returns an instance of
	 * {@link GetUserToFriendsActivity.Request}. You should use this method if
	 * you want to get the list of users whose posts this user (given in input)
	 * engaged in. The activity is wrapped into a {@link ResultContext}. Below
	 * is an example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code List<UserToUserActivity>} pair = resultContext.get(0);
	 * ...
	 * </code>
	 * </pre>
	 * 
	 * @param userId
	 *            User that is engaging with other users posts
	 * @return GetUserToFriendsActivity.Request
	 */
	public static GetUserToFriendsActivity.Request newGetUserToFriendsActivityRequest(
			long userId) {
		return newGetUserToFriendsActivityRequest(userId, null);
	}

	/**
	 * This method returns an instance of
	 * {@link GetUserToFriendsActivity.Request}. You should use this method if
	 * you want to get the list of users whose posts this user (given in input)
	 * engaged in. The activity is wrapped into a {@link ResultContext}. Below
	 * is an example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code List<UserToUserActivity>} pair = resultContext.get(0);
	 * ...
	 * </code>
	 * </pre>
	 * 
	 * @param userId
	 *            User that is engaging with other users posts
	 * @param startDateMillis
	 *            The start date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @param endDateMillis
	 *            The end date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @return GetUserToFriendsActivity.Request
	 */
	public static GetUserToFriendsActivity.Request newGetUserToFriendsActivityRequest(
			long userId, long startDateMillis, long endDateMillis) {
		return newGetUserToFriendsActivityRequest(userId, startDateMillis,
				endDateMillis, null);
	}

	/**
	 * This method returns an instance of
	 * {@link GetUserToFriendsActivity.Request}. You should use this method if
	 * you want to get the list of users who engaged on this user (given in
	 * input)'s posts. engaged in. The activity is wrapped into a
	 * {@link com.sysomos.activity.service.GetUserToFriendsActivity.ResultContext}
	 * . Below is an example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code List<UserToUserActivity>} pair = resultContext.get(0);
	 * ...
	 * </code>
	 * </pre>
	 * 
	 * @param userId
	 *            User whose posts other users engaged on
	 * @param type
	 *            Activity type (@see {@link ActivityService.ActivityType})
	 * @return GetUserToFriendsActivity.Request
	 */
	public static GetUserToFriendsActivity.Request newGetUserToFriendsActivityRequest(
			long userId, ActivityType type) {
		return new GetUserToFriendsActivity.Request(userId, type == null ? null
				: type.descriptor);
	}

	/**
	 * This method returns an instance of
	 * {@link GetUserToFriendsActivity.Request}. You should use this method if
	 * you want to get the list of users who engaged on this user (given in
	 * input)'s posts. engaged in. The activity is wrapped into a
	 * {@link com.sysomos.activity.service.GetUserToFriendsActivity.ResultContext}
	 * . Below is an example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code List<UserToUserActivity>} pair = resultContext.get(0);
	 * ...
	 * </code>
	 * </pre>
	 * 
	 * 
	 * @param userId
	 *            User whose posts other users engaged on
	 * 
	 * @param startDateMillis
	 *            The start date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @param endDateMillis
	 *            The end date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * 
	 * @param type
	 *            Activity type (@see {@link ActivityService.ActivityType})
	 * @return GetUserToFriendsActivity.Request
	 */
	public static GetUserToFriendsActivity.Request newGetUserToFriendsActivityRequest(
			long userId, long startDateMillis, long endDateMillis,
			ActivityType type) {
		return new GetUserToFriendsActivity.Request(userId, type == null ? null
				: type.descriptor, startDateMillis, endDateMillis);
	}

	/**
	 * This method returns an instance of
	 * {@link GetFollowersToUserActivity.Request}. You should use this method if
	 * you want to get the list of users who engaged on this user (given in
	 * input)'s posts. engaged in. The activity is wrapped into a
	 * {@link com.sysomos.activity.service.GetFollowersToUserActivity.ResultContext}
	 * . Below is an example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code List<UserToUserActivity>} pair = resultContext.get(0);
	 * ...
	 * </code>
	 * </pre>
	 * 
	 * 
	 * @param userId
	 *            User whose posts other users engaged on
	 * @return GetFollowersToUserActivity.Request
	 */
	public static GetFollowersToUserActivity.Request newGetFollowersToUserActivityRequest(
			long userId) {
		return newGetFollowersToUserActivityRequest(userId, null);
	}

	/**
	 * This method returns an instance of
	 * {@link GetFollowersToUserActivity.Request}. You should use this method if
	 * you want to get the list of users who engaged on this user (given in
	 * input)'s posts. engaged in. The activity is wrapped into a
	 * {@link com.sysomos.activity.service.GetFollowersToUserActivity.ResultContext}
	 * . Below is an example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code List<UserToUserActivity>} pair = resultContext.get(0);
	 * ...
	 * </code>
	 * </pre>
	 * 
	 * @param userId
	 *            User whose posts other users engaged on
	 * 
	 * @param startDateMillis
	 *            The start date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @param endDateMillis
	 *            The end date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * 
	 * @return GetFollowersToUserActivity.Request
	 */
	public static GetFollowersToUserActivity.Request newGetFollowersToUserActivityRequest(
			long userId, long startDateMillis, long endDateMillis) {
		return newGetFollowersToUserActivityRequest(userId, startDateMillis,
				endDateMillis, null);
	}

	/**
	 * This method returns an instance of
	 * {@link GetFollowersToUserActivity.Request}. You should use this method if
	 * you want to get the list of users who engaged on this user (given in
	 * input)'s posts. engaged in. The activity is wrapped into a
	 * {@link com.sysomos.activity.service.GetFollowersToUserActivityRequest.ResultContext}
	 * . Below is an example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code List<UserToUserActivity>} pair = resultContext.get(0);
	 * ...
	 * </code>
	 * </pre>
	 * 
	 * @param userId
	 *            User whose posts other users engaged on
	 * @param type
	 *            Activity type (@see {@link ActivityService.ActivityType})
	 * @return GetFollowersToUserActivity.Request
	 */
	public static GetFollowersToUserActivity.Request newGetFollowersToUserActivityRequest(
			long userId, ActivityType type) {
		return new GetFollowersToUserActivity.Request(userId,
				type == null ? null : type.descriptor);
	}

	/**
	 * This method returns an instance of
	 * {@link GetUserToFriendsActivity.Request}. You should use this method if
	 * you want to get the list of users who engaged on this user (given in
	 * input)'s posts. engaged in. The activity is wrapped into a
	 * {@link com.sysomos.activity.service.newGetFollowersToUserActivityRequest.ResultContext}
	 * . Below is an example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code List<UserToUserActivity>} pair = resultContext.get(0);
	 * ...
	 * </code>
	 * </pre>
	 * 
	 * @param userId
	 *            User whose posts other users engaged on
	 * 
	 * @param startDateMillis
	 *            The start date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @param endDateMillis
	 *            The end date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * 
	 * @param type
	 *            Activity type (@see {@link ActivityService.ActivityType})
	 * @return GetFollowersToUserActivity.Request
	 */
	public static GetFollowersToUserActivity.Request newGetFollowersToUserActivityRequest(
			long userId, long startDateMillis, long endDateMillis,
			ActivityType type) {
		return new GetFollowersToUserActivity.Request(userId,
				type == null ? null : type.descriptor, startDateMillis,
				endDateMillis);
	}

	/**
	 * This method returns an instance of {@link GetUserToUserActivity.Request}.
	 * You should call this method if you want to know how many times userId1
	 * engaged with userId2 and vice-versa. The activity is wrapped into a
	 * {@link com.sysomos.activity.service.GetUserToUserActivity.ResultContext}.
	 * Below is an example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code UserToUserActivity} activity = resultContext.get(0);
	 * ...
	 * </code>
	 * </pre>
	 * 
	 * @param userId1
	 *            User that is engaging with another user (userId2) post
	 * @param userId2
	 *            User whose posts userId1 is engaging in.
	 * @return UserToUserActivity.Request
	 */
	public static GetUserToUserActivity.Request newGetUserToUserActivityRequest(
			long userId1, long userId2) {
		return newGetUserToUserActivityRequest(userId1, userId2, null);
	}

	/**
	 * This method returns an instance of {@link GetUserToUserActivity.Request}.
	 * You should call this method if you want to know how many times userId1
	 * engaged with userId2 and vice-versa. The activity is wrapped into a
	 * {@link com.sysomos.activity.service.GetUserToUserActivity.ResultContext}.
	 * Below is an example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code UserToUserActivity} activity = resultContext.get(0);
	 * ...
	 * </code>
	 * </pre>
	 * 
	 * @param userId1
	 *            User that is engaging with another user post
	 * @param userId2
	 *            User whose posts userId1 is engaging in.
	 * @param startDateMillis
	 *            The start date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @param endDateMillis
	 *            The end date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @return UserToUserActivity.Request
	 */
	public static GetUserToUserActivity.Request newGetUserToUserActivityRequest(
			long userId1, long userId2, long startDateMillis, long endDateMillis) {
		return newGetUserToUserActivityRequest(userId1, userId2,
				startDateMillis, endDateMillis, null);
	}

	/**
	 * This method returns an instance of {@link GetUserToUserActivity.Request}.
	 * You should call this method if you want to know how many times userId1
	 * engaged with userId2 and vice-versa. The activity is wrapped into a
	 * {@link com.sysomos.activity.service.GetUserToUserActivity.ResultContext}.
	 * Below is an example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code UserToUserActivity} activity = resultContext.get(0);
	 * ...
	 * </code>
	 * </pre>
	 * 
	 * @param userId1
	 *            User that is engaging with another user post
	 * @param userId2
	 *            User whose posts userId1 is engaging in.
	 * @param type
	 *            Activity type (@see {@link ActivityService.ActivityType})
	 * @return UserToUserActivity.Request
	 */
	public static GetUserToUserActivity.Request newGetUserToUserActivityRequest(
			long userId1, long userId2, ActivityType type) {
		return new GetUserToUserActivity.Request(ListUtils.newList(userId1,
				userId2), type == null ? null : type.descriptor);
	}

	/**
	 * This method returns an instance of {@link GetUserToUserActivity.Request}.
	 * You should call this method if you want to know how many times userId1
	 * engaged with userId2 and vice-versa. The activity is wrapped into a
	 * {@link com.sysomos.activity.service.GetUserToUserActivity.ResultContext}.
	 * Below is an example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code UserToUserActivity} activity = resultContext.get(0);
	 * ...
	 * </code>
	 * </pre>
	 * 
	 * @param userId1
	 *            User that is engaging with another (userId2) user post
	 * @param userId2
	 *            User whose posts userId1 is engaging in.
	 * @param startDateMillis
	 *            The start date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @param endDateMillis
	 *            The end date in milliseconds for the time window to consider
	 *            when querying the user activity table.
	 * @param type
	 *            Activity type (@see {@link ActivityService.ActivityType})
	 * @return UserToUserActivity.Request
	 */
	public static GetUserToUserActivity.Request newGetUserToUserActivityRequest(
			long userId1, long userId2, long startDateMillis,
			long endDateMillis, ActivityType type) {
		return new GetUserToUserActivity.Request(ListUtils.newList(userId1,
				userId2), type == null ? null : type.descriptor,
				startDateMillis, endDateMillis);
	}

	/**
	 * This method returns an instance of {@link GetUserActivity.Batch}. This
	 * method should be called only when you want have a collection of userIds
	 * and want to fetch their activities. Below is an example of how to fetch
	 * the activity.
	 * 
	 * <pre>
	 * <code>
	 * GetUserActivity.Batch batch = ActivityService.newGetUserActivityBatch();
	 * </code>
	 * Then you can either pass the batch a list of userIds or use inside a loop as per below
	 * <code>
	 * for (long id :  userIds) {
	 *    batch.addRequest(id);
	 * }
	 * </code>
	 * </pre>
	 * 
	 * @return GetUserActivity.Batch
	 */
	public static GetUserActivity.Batch newGetUserActivityBatch() {
		return new GetUserActivity.Batch(BATCH_SIZE);
	}

	/**
	 * This method returns an instance of
	 * {@link GetFollowersToUserActivity.Batch}. This method should be called
	 * only when you want have a collection of userIds and want to fetch
	 * activities of others users who engaged on these users' posts. Below is an
	 * example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * GetFollowersToUserActivity.Batch batch = ActivityService.newGetFollowersToUserActivityBatch();
	 * </code>
	 * Then you can either pass the batch a list of userIds or use inside a loop as per below
	 * <code>
	 * for (long id :  userIds) {
	 *    batch.addRequest(id);
	 * }
	 * </code>
	 * </pre>
	 * 
	 * @return GetFollowersToUserActivity.Batch
	 */
	public static GetFollowersToUserActivity.Batch newGetFollowersToUserActivityBatch() {
		return new GetFollowersToUserActivity.Batch(BATCH_SIZE);
	}

	/**
	 * This method returns an instance of {@link GetUserToFriendsActivity.Batch}
	 * . This method should be called only when you want have a collection of
	 * userIds and want to fetch their activities. Below is an example of how to
	 * fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * GetUserToFriendsActivity.Batch batch = ActivityService.newGetUserToFriendsActivityBatch();
	 * </code>
	 * Then you can either pass the batch a list of userIds or use inside a loop as per below
	 * <code>
	 * for (long id :  userIds) {
	 *    batch.addRequest(id);
	 * }
	 * </code>
	 * </pre>
	 * 
	 * @return GetUserActivity.Batch
	 */
	public static GetUserToFriendsActivity.Batch newGetUserToFriendsActivityBatch() {
		return new GetUserToFriendsActivity.Batch(BATCH_SIZE);
	}

	/**
	 * This method returns an instance of {@link GetUserToUserActivity.Batch}.
	 * This method should be called only when you want have a collection of
	 * userIds and want to fetch activities of others users who engaged on these
	 * users' posts. Below is an example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * GetUserToUserActivity.Batch batch = ActivityService.newGetUserToUserActivityBatch();
	 * </code>
	 * Then you can either pass the batch a list of userIds or use inside a loop as per below
	 * <code>
	 * for (long id :  userIds) {
	 *    batch.addRequest(id);
	 * }
	 * </code>
	 * </pre>
	 * 
	 * @return GetUserToUserActivity.Batch
	 */
	public static GetUserToUserActivity.Batch newGetUserToUserActivityBatch() {
		return new GetUserToUserActivity.Batch(BATCH_SIZE);
	}

	/**
	 * This method returns an instance of {@link GetUserToUserActivity.Batch}.
	 * This method should be called only when you want have a collection of
	 * userIds and want to fetch activities of others users who engaged on these
	 * users' posts. Below is an example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * GetUserToUserActivity.Batch batch = ActivityService.newGetUserToUserActivityBatch();
	 * </code>
	 * Then you can either pass the batch a list of userIds or use inside a loop as per below
	 * <code>
	 * for (long id :  userIds) {
	 *    batch.addRequest(id);
	 * }
	 * </code>
	 * </pre>
	 * 
	 * @param batchSize
	 * @return GetUserToUserActivity.Batch
	 */
	public static GetUserToUserActivity.Batch newGetUserToUserActivityBatch(
			int batchSize) {
		return new GetUserToUserActivity.Batch(batchSize);
	}
}
