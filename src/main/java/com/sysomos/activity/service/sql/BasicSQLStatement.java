package com.sysomos.activity.service.sql;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BasicSQLStatement {

	private static final Logger LOG = LoggerFactory
			.getLogger(BasicSQLStatement.class);
	protected Connection connection = null;
	protected PreparedStatement statement = null;
	private static final String CONFIG_FILE = "config.properties";

	/**
	 * Constructor
	 * 
	 * @param sqlQuery
	 *            SQL Query to execute
	 */
	public BasicSQLStatement(String sqlQuery) {
		initialize(sqlQuery, null);
	}

	/**
	 * Constructor
	 * 
	 * @param sqlQuery
	 *            SQL Query to execute
	 * @param parameters
	 *            Parameters of the SQL Query. These are used in a prepared
	 *            statement to replace "?" in the where clause of the SQL Query
	 */
	public BasicSQLStatement(String sqlQuery, Object... parameters) {
		initialize(sqlQuery, parameters);
	}

	private void initialize(String sqlQuery, Object[] queryParams) {
		if (StringUtils.isEmpty(sqlQuery))
			return;
		Properties prop = new Properties();

		InputStream inputStream = getClass().getClassLoader()
				.getResourceAsStream(CONFIG_FILE);
		try {
			prop.load(inputStream);
		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage());
			return;
		}

		String user = prop.getProperty("user");
		String pwd = prop.getProperty("password");
		String dbUrl = prop.getProperty("url");

		try {
			connection = DriverManager.getConnection(dbUrl, user, pwd);
			statement = connection.prepareStatement(sqlQuery);
			if (queryParams == null)
				return;
			int parameterIndex = 1;
			for (Object param : queryParams) {
				if (param instanceof Long) {
					statement.setLong(parameterIndex++,
							((Long) param).longValue());
				} else if (param instanceof String) {
					statement.setString(parameterIndex++, param.toString());
				} else if (param instanceof Integer) {
					statement.setLong(parameterIndex++,
							((Integer) param).intValue());
				} else if (param instanceof Float) {
					statement.setFloat(parameterIndex++,
							((Float) param).intValue());
				} else {

				}
			}
			//System.out.println(statement.toString());
		} catch (SQLException e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

	public PreparedStatement prepareStatement() {
		return statement;
	}
}
